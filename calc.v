module top (
	output OP1_LED0,
	output OP1_LED1,
	output OP1_LED2,
	output OP1_LED3,

	output OP2_LED0,
	output OP2_LED1,
	output OP2_LED2,
	output OP2_LED3,

	output OUT_LED0,
	output OUT_LED1,
	output OUT_LED2,
	output OUT_LED3,

	output LED_DIV,
	output LED_MUL,
	output LED_ADD,
	output LED_SUB,

	input BUTTON_RETURN,
	input BUTTON_FUNC,
	input BUTTON_OP1,
	input BUTTON_OP2,

	input SWITCH0,
	input SWITCH1,
	input SWITCH2,
	input SWITCH3,

	input clk

);

	reg [3:0] op1, op2, out, in;
	reg [4:0] func = 1;
	reg [14:0] counter = 0;
	reg flag = 1;

	always @(posedge clk) begin
		counter <= counter + 1;

	end

	always @(negedge counter[14:14]) begin
		if (BUTTON_FUNC == 0 && flag == 1) begin
			func <= func + func + func[4:4];
			flag <= 0;
		end else if (BUTTON_FUNC == 1 && flag == 0) begin
			flag <= 1;
		end
	end

/*
	always @(negedge BUTTON_FUNC) begin
		func <= func + func + func[4:4];
		flag <= 0;
	end

	always @(posedge BUTTON_FUNC) begin
		flag <= 1;
	end	
*/
	always @(negedge BUTTON_OP1) begin
		op1 <= in;
	end

	always @(negedge BUTTON_OP2) begin
		op2 <= in;
	end

	always @(negedge BUTTON_RETURN) begin
		case (func[3:0])
			4'b0001 : out <= op1 * op2;
			4'b0010 : out <= op1 - op2;
			4'b0100 : out <= op1 / op2;
			4'b1000 : out <= op1 + op2;
			default : out <= 4'b0000;
		endcase
	end


	assign {OP1_LED3, OP1_LED2, OP1_LED1, OP1_LED0} = op1;
	assign {OP2_LED3, OP2_LED2, OP2_LED1, OP2_LED0} = op2;
	assign {OUT_LED3, OUT_LED2, OUT_LED1, OUT_LED0} = out;
	assign {LED_ADD, LED_MUL, LED_SUB, LED_DIV} = func;
	assign in = {SWITCH3, SWITCH2, SWITCH1, SWITCH0};

endmodule
